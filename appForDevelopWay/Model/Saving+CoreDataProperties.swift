//
//  Saving+CoreDataProperties.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//
//

import Foundation
import CoreData

extension Saving {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Saving> {
        return NSFetchRequest<Saving>(entityName: "Saving")
    }
    
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var password: String?
    @NSManaged public var repass: String?
    @NSManaged public var imageD: Data?
    @NSManaged public var profileImg: Data?
}

extension Saving : Identifiable {

}
