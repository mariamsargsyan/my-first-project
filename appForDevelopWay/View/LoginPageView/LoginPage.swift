//
//  LoginPage.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//


import SwiftUI
import CoreData
import FirebaseCore
import FirebaseAuth

struct LoginPage: View {
    @Environment(\.managedObjectContext)  var viewContext
    
    @FetchRequest(entity: Saving.entity(),sortDescriptors:
                    [NSSortDescriptor(keyPath: \Saving.name, ascending: true),
                     NSSortDescriptor(keyPath: \Saving.email, ascending: true),
                     NSSortDescriptor(keyPath: \Saving.imageD, ascending: false),
                     NSSortDescriptor(keyPath: \Saving.password, ascending: true)
                    ])
    var saving: FetchedResults<Saving>
    @State var color = Color.black.opacity(0.7)
    @State var visible: Bool = false
    @State var email: String = ""
    @State var name: String = ""
    @State var password: String = ""
    @State var show: Bool = false
    @State var showLogin: Bool = false
    @State var showForgetPass: Bool = false
    @State var error = ""
    @State var alert = false
    @State var selection4: String? = ""
    @State var image: Data = .init(count: 0)
    @State var profileImage: Data = .init(count: 0)
    @State var selection3: String? = ""
    
    var body: some View {
        NavigationView {
            ZStack {
                ZStack(alignment: .center) {
                    GeometryReader { _ in
                        VStack {
                            
                           travelImage
                            
                           EmailTF(email: $email)
                            
                            Password(visible: $visible, password: $password)
                            .modifier(PassModyfier(password: $password))
                            
                            NavigationLink(destination: DetailView(), tag: "act3", selection: $selection3) {
                                
                                Button(action: {
                                    verify()
                                }) {
                                   loginText
                                }
                                .modifier(ButtonModyfier())
                            }
                            
                            Button(action: {
                                reset()
                            }) {
                                forgotPass
                            }
                        }
                        .navigationBarItems(trailing: RegisterButtn(show: $show))
                    }.sheet(isPresented: $show) {
                        Registerpage().environment(\.managedObjectContext, viewContext)
                    }
                }
                if self.alert {
                    ErrorView(alert: $alert, error: $error)
                        .offset( y: -28)
                }
            }
        }
        .navigationBarBackButtonHidden(true)
    }

    func reset() {
        if self.email != "" {
            Auth.auth().sendPasswordReset(withEmail: self.email) { err in
                if err != nil {
                    self.error = err!.localizedDescription
                    self.alert.toggle()
                    return
                }
                self.error = "RESET"
                self.alert.toggle()
            }
        }
        else {
            self.error = "Email ID is empty"
            self.alert.toggle()
        }
    }
    func verify() {
        if self.email != "" && self.password != "" {
            Auth.auth().signIn(withEmail: self.email, password: self.password) { res, err in
                if err != nil {
                    self.error = err!.localizedDescription
                    self.alert.toggle()
                    return
                }
                selection3 = "act3"
                email = ""
                password = ""
                NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
            }
        } else {
            self.error = "Please fill all the contents properly"
            self.alert.toggle()
        }
    }
}

private var travelImage: some View {
    Image("travel")
        .renderingMode(.original)
        .resizable()
        .frame(width: UIScreen.main.bounds.width - 230, height: 150)
}

struct EmailTF: View {
    @Binding var email: String
    var body: some View {
        TextField("Email", text: $email)
            .modifier(EmailtEXTfieldModifyer( email: $email))
    }
}

struct Password: View {
    @State var color = Color.black.opacity(0.7)
    @Binding var visible: Bool
    @Binding var password: String
    var body: some View {
        HStack(spacing: 15){
            VStack{
                if self.visible{
                    TextField("Password", text: self.$password)
                        .autocapitalization(.none)
                }
                else{
                    SecureField("Password", text: self.$password)
                        .autocapitalization(.none)
                }
            }
            Button(action: {
                
                self.visible.toggle()
                
            }) {
                Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                    .foregroundColor(self.color)
            }
        }
    }
}

private var loginText: some View {
    Text("Log In")
        .foregroundColor(.white)
        .padding(.vertical)
        .frame(width: UIScreen.main.bounds.width - 50)
}

private var forgotPass: some View {
    Text("Forget password?")
        .foregroundColor(Color("Color"))
}

struct RegisterButtn: View {
    @Binding var show: Bool
    var body: some View {
        Button(action: {
            show.toggle()
        }) {
            Text("Register")
                .fontWeight(.bold)
                .foregroundColor(Color("Color"))
        }
    }
}
