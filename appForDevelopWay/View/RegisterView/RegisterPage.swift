//
//  RegisterPage.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//

import SwiftUI
import FirebaseCore
import FirebaseAuth

struct Registerpage: View {
    
    @Environment(\.managedObjectContext)  var viewContext
    @Environment(\.presentationMode)  var dismiss
    @State var alert = false
    @State var color = Color.black.opacity(0.7)
    @State var error = ""
    @State var showingProfile: Bool = false
    @State var nameTF: String = ""
    @State var emailTF: String = ""
    @State var passTF: String = ""
    @State var repassTF: String = ""
    @State var visible: Bool = false
    @State var isSignup: Bool = false
    @State var selection: String?
    @State var image: Data = .init(count: 0)
    @State var profileImage: Data = .init(count: 0)
    @State var sourcetype: UIImagePickerController.SourceType = .photoLibrary
    @State var show: Bool = false
    
    var body: some View {
        NavigationView {
            ZStack {
                ZStack(alignment: .trailing) {
                    GeometryReader { _ in
                        ScrollView(.vertical, showsIndicators: false) {
                            NavigationLink(destination: SuccessPage(image: $image, profileImage: $profileImage, nameTF: $nameTF, emailTF: $emailTF), tag: "act", selection: $selection) {
                                EmptyView()
                            }
                            VStack {
                               
                                Images(show: $show, image: $image)
                                
                                NameTextField(nameTF: $nameTF)
                                
                                NameTFValidation(nameTF: $nameTF)
                                
                                EmailTextField(emailTF: $emailTF)
                                
                                EmailTFValidation(emailTF: $emailTF)
                                
                                RadioButtons()
                                
                                Group {
                                    
                                    PasswordTF(passTF: $passTF, visible: $visible)
                                    
                                    RePasswordTF(repassTF: $repassTF, visible: $visible)
                                }
                               
                                PassValidation(passTF: $passTF)
                                
                                SignupValidation(isSignup: $isSignup)
                                
                                Button(action: {
                                    register()
                                }) {
                                    signupTxt
                                        .foregroundColor(isSignup ? .gray : Color("Color"))
                                }
                            }.sheet(isPresented: $showingProfile) {
                                ImagePicker(images: $profileImage, show: $showingProfile, sourceType: sourcetype)
                            }
                            
                        }.sheet(isPresented: $show, content: {
                            ImagePicker(images: $image, show: $show, sourceType: sourcetype)
                        })
                            .navigationBarTitle("Sign Up")
                            .navigationBarItems(trailing: ProfileImage(showingProfile: $showingProfile, profileImage: $profileImage))
                    }
                }
                if self.alert {
                    ErrorView(alert: $alert, error: $error)
                        .offset( y: -28)
                }
            }
        }
    }
    func register(){
        if nameTF.isEmpty || emailTF.isEmpty || passTF.isEmpty || image.count == 0 || profileImage.count == 0 {
            isSignup = true
        } else {
            isSignup = false
        }
        if self.emailTF != ""{
            
            if self.passTF == self.repassTF{
                
                Auth.auth().createUser(withEmail: self.emailTF, password: self.passTF) { (res, err) in
                    
                    if err != nil{
                        
                        self.error = err!.localizedDescription
                        self.alert.toggle()
                        return
                    }
                    print("success")
                    let add = Saving(context: viewContext)
                    add.name = nameTF
                    add.email = emailTF
                    add.password = passTF
                    add.imageD = image
                    add.profileImg = profileImage
                    add.repass = repassTF
                    try! viewContext.save()
                    selection = "act"
                    NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
                }
            }
            else{
                self.error = "Password mismatch"
                self.alert.toggle()
            }
        }
        else{
            self.error = "Please fill all the contents properly"
            self.alert.toggle()
        }
    }
}

struct Images: View {
    @Binding var show: Bool
    @Binding var image: Data
    var body: some View {
        if self.image.count != 0 {
            Button {
            } label: {
                Image(uiImage: UIImage(data: self.image)!)
                    .ImageModifier()
            }
        } else {
            Button {
                show.toggle()
            } label: {
                Image(systemName: "photo.fill")
                    .PhotoImageModifier()
            }
        }
    }
}

struct NameTextField: View {
    @Binding var nameTF: String
    var body: some View {
        TextField("Name", text: $nameTF)
            .modifier(EmailtEXTfieldModifyer(email: $nameTF))
    }
}

struct NameTFValidation: View {
    @Binding var nameTF: String
    var body: some View {
        if !nameTF.isEmpty  && !nameTF.isValidName {
            Text("Name should start with uppercase and contain only letters")
                .font(.subheadline)
                .foregroundColor(.red)
        }
    }
}

struct EmailTextField: View {
    @Binding var emailTF: String
    var body: some View {
        TextField("Email", text: $emailTF)
            .modifier(EmailtEXTfieldModifyer( email: $emailTF))
    }
}

struct EmailTFValidation: View {
    @Binding var emailTF: String
    var body: some View {
        if !emailTF.isEmpty && !emailTF.isValidEmail {
            Text("Incorrect email format")
                .font(.subheadline)
                .foregroundColor(.red)
        }
    }
}

struct PasswordTF: View {
    @Binding var passTF: String
    @Binding var visible: Bool
    @State var color = Color.black.opacity(0.7)
    var body: some View {
        HStack(spacing: 15){
            VStack{
                if self.visible{
                    TextField("Password", text: self.$passTF)
                        .autocapitalization(.none)
                }
                else{
                    SecureField("Password", text: self.$passTF)
                        .autocapitalization(.none)
                }
            }
            Button(action: {
                self.visible.toggle()
            }) {
                Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                    .foregroundColor(self.color)
            }
        }
        .modifier(PasswordModyfier(passTF: $passTF))
    }
}

struct RePasswordTF: View {
    @Binding var repassTF: String
    @Binding var visible: Bool
    @State var color = Color.black.opacity(0.7)
    var body: some View {
        HStack(spacing: 15){
            VStack{
                if self.visible{
                    TextField("Re-enter password", text: self.$repassTF)
                        .autocapitalization(.none)
                }
                else{
                    SecureField("Re-enter password", text: self.$repassTF)
                        .autocapitalization(.none)
                }
            }
            Button(action: {
                self.visible.toggle()
            }) {
                Image(systemName: self.visible ? "eye.slash.fill" : "eye.fill")
                    .foregroundColor(self.color)
            }
        }
        .modifier(RePasswordModyfier(repassTF: $repassTF))
    }
}

struct PassValidation: View {
    @Binding var passTF: String
    var body: some View {
        if !passTF.isEmpty && !passTF.isValidPassword {
            Text("Password should contain min 8 characters, one upper and lower case and one special character")
                .modifier(PassValidationModyfier())
        }
    }
}

struct SignupValidation: View {
    @Binding var isSignup: Bool
    var body: some View {
        if isSignup {
            
            Text("Please fill all the fields.")
                .foregroundColor(.red)
        } else {
            Text(" ")
        }
    }
}

private var signupTxt: some View {
    Text("Sign Up")
       
}

struct ProfileImage: View {
    @Binding var showingProfile: Bool
    @Binding var profileImage: Data
    var body: some View {
        HStack { if self.profileImage.count != 0 { Button(action: {
            self.showingProfile.toggle()
        })  {
            Image(uiImage: UIImage(data: self.profileImage)!)
                .ProfImageModifier()
            
        }} else {
            Button {
                self.showingProfile.toggle()
            } label: {
                Image(systemName: "person.circle.fill")
                    .ProfileImageModifier()
            }
        }}
    }
}
