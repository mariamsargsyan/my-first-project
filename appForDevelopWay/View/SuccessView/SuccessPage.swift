//
//  SuccessPage.swift
//  appForDevelopWay
//
//  Created by Apple on 08.10.21.
//

import SwiftUI

struct SuccessPage: View {
    
    @Environment(\.managedObjectContext)  var viewContext
    @Binding var image: Data
    @Binding var profileImage: Data
    @State var isSuccess: Bool = false
    @State var selection3: String? = ""
    @Binding var nameTF: String
    @Binding var emailTF: String
    @FetchRequest(entity: Saving.entity(),
                  sortDescriptors: [NSSortDescriptor(keyPath: \Saving.name, ascending: true),
                                    NSSortDescriptor(keyPath: \Saving.email, ascending: true),
                                    NSSortDescriptor(keyPath: \Saving.imageD, ascending: false),
                                    NSSortDescriptor(keyPath: \Saving.password, ascending: true),
                                    
                                   ])
    var saving: FetchedResults<Saving>
    var sourcetype: UIImagePickerController.SourceType = .photoLibrary
    
    var body: some View {
        VStack {
            
            congrats
            
            success
            
            NavigationLink(destination: DetailView(dImg: image, dProfi: profileImage, email: emailTF, name: nameTF), tag: "act3", selection: $selection3) {
                
                Button(action: {
                    isSuccess.toggle()
                    selection3 = "act3"
                }) {
                    goProfile
                }
                .modifier(ButtonModyfier())
            }
        }.navigationBarBackButtonHidden(true)
    }
}

private var goProfile: some View {
    Text("Go to my profile")
        .foregroundColor(.white)
        .padding(.vertical)
        .frame(width: UIScreen.main.bounds.width - 50)
}

private var congrats: some View {
    Text("Congrats!")
}

private var success: some View {
    Text("You have succesfully registered!")
}
