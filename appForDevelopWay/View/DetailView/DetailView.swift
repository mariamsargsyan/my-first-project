//
//  DetailView.swift
//  appForDevelopWay
//
//  Created by Apple on 08.10.21.
//
import SwiftUI
import CoreData
import FirebaseCore
import FirebaseAuth

struct DetailView: View {
    
    @Environment(\.managedObjectContext)  var viewContext
    @Environment(\.presentationMode)  var dismiss
    @State var detail = Saving()
    @State var dImg: Data = .init(count: 0)
    @State var dProfi: Data = .init(count: 0)
    @State var email: String = ""
    @State var name: String = ""
    @State var logOut: Bool = false
    @State var image: Data = .init(count: 0)
    @State var profileImage: Data = .init(count: 0)
    @State var selection5: String? = ""
    @FetchRequest(entity: Saving.entity(),
                  sortDescriptors: [NSSortDescriptor(keyPath: \Saving.name, ascending: true),
                                    NSSortDescriptor(keyPath: \Saving.email, ascending: true),
                                    NSSortDescriptor(keyPath: \Saving.imageD, ascending: false),
                                    NSSortDescriptor(keyPath: \Saving.password, ascending: true)
                                   ])
    var saving: FetchedResults<Saving>
    
    var body: some View {
        ScrollView {
            ForEach(saving, id: \.self) { indexRow in
                VStack(alignment: .leading, spacing: 20) {
                    ZStack {
                        Image(uiImage: UIImage(data: indexRow.imageD ?? self.dImg) ?? UIImage())
                            .DetailImageModifier()
                        
                        Image(uiImage: UIImage(data: indexRow.profileImg ?? self.dProfi) ?? UIImage())
                            .DetailProfileImageModifier()
                    }
                    
                    VStack(alignment: .leading, spacing: 25) {
                        Section(header: Text("Name")) {
                            Text("\(indexRow.name ?? self.name)")
                                .frame(width: UIScreen.main.bounds.width)
                                .modifier(ButtonModyfiers())
                        }
                        Section(header: Text("Email")) {
                            Text("\(indexRow.email ?? self.email)")
                                .frame(width: UIScreen.main.bounds.width)
                                .modifier(ButtonModyfiers())
                        }
                    }
                }
                .navigationBarBackButtonHidden(true)
                .navigationBarTitle("Welcome!", displayMode: .inline)
                .navigationBarItems(trailing:  NavigationLink(destination: LoginPage(), tag: "act5", selection: $selection5) {
                    
                 Button(action: {

                    try! Auth.auth().signOut()
                     dismiss.wrappedValue.dismiss()
                     dismiss.wrappedValue.dismiss()
                    NotificationCenter.default.post(name: NSNotification.Name("status"), object: nil)
   
                }) {
                    Text("Log Out")
                        .foregroundColor(.blue)
                }})
            }
        }
    }
}
