//
//  RadioButtons.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//

import SwiftUI

struct RadioButtons: View {
    
    var gender = ["Male", "Female", "Other"]
    @State var selectedButton: Int?
    
    var body: some View {
        HStack(spacing: 15) {
            ForEach(0..<gender.count) { gen in
                Button(action: {
                    selectedButton = gen
                }) {
                    Text("\(gender[gen])")
                        .modifier(RadioButtonModyfier())
                        .background(self.selectedButton == gen ? Color("Color") : Color(#colorLiteral(red: 0.926646173, green: 0.7299426794, blue: 0.8714609742, alpha: 1)))
                }
            }
        }
    }
}
