//
//  ErrorView.swift
//  appForDevelopWay
//
//  Created by Apple on 08.10.21.
//

import SwiftUI
import FirebaseCore
import FirebaseAuth

struct ErrorView : View {
    
    @State var color = Color.black.opacity(0.7)
    @Binding var alert : Bool
    @Binding var error : String
    
    var body: some View{
        
        GeometryReader{_ in
            
            VStack{
                
                HStack{
                    
                    Text(self.error == "RESET" ? "Message" : "Error")
                        .modifier(MessageModyfier())
                    
                    Spacer()
                }
                .padding(.horizontal, 25)
                
                Text(self.error == "RESET" ? "Password reset link has been sent successfully" : self.error)
                    .modifier(ResetLinkModyfier())
                
                Button(action: {
                    
                    self.alert.toggle()
                    
                }) {
                    
                    Text(self.error == "RESET" ? "Ok" : "Cancel")
                        .modifier(ResetModyfier())
                }
                .modifier(ButtonModyfier())
                
            }
            .modifier(ErrorModyfier())
        }
        .background(Color.black.opacity(0.35).edgesIgnoringSafeArea(.all))
    }
}
