//
//  Extension+validation.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//


import Foundation

extension String {
    
    var isValidEmail: Bool {
        let lowercasdEmail = self.lowercased()
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        let result = test.evaluate(with: lowercasdEmail)
        return result
    }
    
    var isValidName: Bool {
        let name = self.lowercased()
        let regex = "[A-Za-z- ]{2,}"
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        let result = test.evaluate(with: name)
        return result
    }
    
    var isValidPhoneNumber: Bool{
        let PHONE_REGEX = "^[+][0-9]{3,20}$";
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self)
        return result

    }
    var isValidPassword: Bool{
        let password = "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}";
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", password)
        let result =  passwordTest.evaluate(with: self)
        return result
        
    }
}

