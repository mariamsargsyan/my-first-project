//
//  Modifyers.swift
//  appForDevelopWay
//
//  Created by Apple on 07.10.21.
//

import Foundation
import SwiftUI

struct EmailtEXTfieldModifyer: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    @Binding var email: String
    func body(content: Content) -> some View {
        content
            .autocapitalization(.none)
            .padding()
            .background(RoundedRectangle(cornerRadius: 4).stroke(self.email != "" ? Color("Color") : self.color, lineWidth: 2))
            .padding()
    }
}
struct ButtonModyfier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .background(Color("Color"))
            .cornerRadius(10)
            .padding()
    }
}

struct ButtonModyfiers: ViewModifier {
    func body(content: Content) -> some View {
        content
        
            .cornerRadius(10)
            .padding(.vertical, 10)
            .background(Color.black.opacity(0.1))
    }
}

struct PassModyfier: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    @Binding var password: String
    func body(content: Content) -> some View {
        content
        
            .padding()
            .background(RoundedRectangle(cornerRadius: 4).stroke(self.password != "" ? Color("Color") : self.color, lineWidth: 2))
            .padding()
    }
}

struct ErrorModyfier: ViewModifier {
    func body(content: Content) -> some View {
        content
        
            .padding(.vertical, 25)
            .frame(width: UIScreen.main.bounds.width - 70)
            .background(Color.white)
            .cornerRadius(15)
    }
}

struct ResetModyfier: ViewModifier {
    func body(content: Content) -> some View {
        content
        
            .foregroundColor(.white)
            .padding(.vertical)
            .frame(width: UIScreen.main.bounds.width - 120)
    }
}

struct ResetLinkModyfier: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    func body(content: Content) -> some View {
        content
        
            .foregroundColor(self.color)
            .padding(.top)
            .padding(.horizontal, 25)
    }
}

struct MessageModyfier: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    func body(content: Content) -> some View {
        content
            .font(.title)
            .foregroundColor(self.color)
    }
}

extension Image {
    func ImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .frame(width: 120, height: 120)
            .cornerRadius(10)
            .shadow(radius: 4)
    }
}

extension Image {
    func PhotoImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .frame(width: 170, height: 120)
            .foregroundColor(Color(#colorLiteral(red: 0.926646173, green: 0.7299426794, blue: 0.8714609742, alpha: 1)))
    }
}

struct PasswordModyfier: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    @Binding var passTF: String
    func body(content: Content) -> some View {
        content
        
            .padding()
            .background(RoundedRectangle(cornerRadius: 4).stroke(self.passTF != "" ? Color("Color") : self.color,lineWidth: 2))
            .padding()
    }
}

struct RePasswordModyfier: ViewModifier {
    @State var color = Color.black.opacity(0.7)
    @Binding var repassTF: String
    func body(content: Content) -> some View {
        content
        
            .padding()
            .background(RoundedRectangle(cornerRadius: 4).stroke(self.repassTF != "" ? Color("Color") : self.color,lineWidth: 2))
            .padding()
    }
}

struct PassValidationModyfier: ViewModifier {
    func body(content: Content) -> some View {
        content
        
            .font(.subheadline)
            .foregroundColor(.red)
            .lineLimit(2)
    }
}

extension Image {
    func ProfImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .frame(width: 35, height: 35)
            .clipShape(Circle())
    }
}

extension Image {
    func ProfileImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .frame(width: 40, height: 40)
            .foregroundColor(Color(#colorLiteral(red: 0.926646173, green: 0.7299426794, blue: 0.8714609742, alpha: 1)))
    }
}

extension Image {
    func DetailImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .frame(height: 270)
    }
}

extension Image {
    func DetailProfileImageModifier() -> some View {
        self
            .renderingMode(.original)
            .resizable()
            .clipShape(Circle())
            .frame(width: 120, height: 120)
            .offset(x: -0, y: 120)
    }
}

struct RadioButtonModyfier: ViewModifier {
    func body(content: Content) -> some View {
        content
        
            .padding(.vertical, 10)
            .padding(.horizontal, 32)
            .foregroundColor(.white)
            .cornerRadius(4)
    }
}
